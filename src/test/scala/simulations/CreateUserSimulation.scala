package simulations

import config.Config
import io.gatling.core.Predef.{Simulation, _}
import scenarios.CreateUserScenario

class CreateUserSimulation extends Simulation {
  private val createUserExec = CreateUserScenario.createUserScenario.inject(atOnceUsers(Config.users))
  setUp(createUserExec)
}
