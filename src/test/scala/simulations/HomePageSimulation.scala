package simulations

import config.Config
import io.gatling.core.Predef.{Simulation, _}
import scenarios.HomePageScenario

class HomePageSimulation extends Simulation {
  private val homePageExec = HomePageScenario.homepage.inject(atOnceUsers(Config.users))
  setUp(homePageExec)
}
