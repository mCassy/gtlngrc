package simulations

import config.Config
import io.gatling.core.Predef.{Simulation, _}
import scenarios.SearchRaceScenario

class SearchRaceSimulation extends Simulation {
  private val searchRace = SearchRaceScenario.search.inject(atOnceUsers(Config.users))
  setUp(searchRace)
}
