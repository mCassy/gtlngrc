package config

object Config {
  val app_url = "http://dev.race.se:8081"

  val users = Integer.getInteger("users", 2).toInt
  val rampUp = Integer.getInteger("rampup", 1).toInt
  val throughput = Integer.getInteger("throughput", 100).toInt
}
