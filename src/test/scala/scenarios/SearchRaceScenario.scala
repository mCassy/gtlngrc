package scenarios

import io.gatling.core.Predef.scenario
import requests._


object SearchRaceScenario {
  val search = scenario("Search race scenario")
    .exec(SportsListRequest.get_sport_list)
    .exec(TotalDistancesRequest.get_total_distances)
    .exec(ArticlesRequest.get_articles)
    .exec(RacesRequest.get_races)
    .exec(SearchLocationsRequest.get_locations)
    .exec(SearchRaceRequest.get_race)
    .exec(RegisterRequest.get_token)
    .exec(LoginRequest.get_profile)
    .exec(OpenRaceRequest.get_one_race)
}
