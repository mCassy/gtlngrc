package scenarios

import io.gatling.core.Predef.scenario
import requests._


object HomePageScenario {
  val homepage = scenario("Home page scenario")
    .exec(SportsListRequest.get_sport_list)
    .exec(TotalDistancesRequest.get_total_distances)
    .exec(ArticlesRequest.get_articles)
    .exec(RacesRequest.get_races)
}
