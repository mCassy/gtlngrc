package scenarios

import io.gatling.core.Predef.scenario
import requests._

import scala.concurrent.duration._

object CreateUserScenario {

  val createUserScenario = scenario("Register user scenario")
    .exec(ArticlesRequest.get_articles)
    .exec(RacesRequest.get_races).pause(500.milliseconds)
    .exec(RegisterRequest.get_token).pause(500.milliseconds)
    .exec(UpdateProfileRequest.get_profile).pause(500.milliseconds)
    .exec(LoginRequest.get_profile)
}
