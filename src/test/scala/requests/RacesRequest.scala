package requests

import config.Config.app_url
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object RacesRequest {
  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "accept-encoding" -> "gzip, deflate, br",
    "locale" -> "en"
  )
  val get_races = http("Home Races")
    .get(app_url + "/api/v1/web/races?limit=8&with=distances&race_date=2020-4-13&orderBy=race_date&sortedBy=asc&search=race_date:2020-4-13&searchFields=race_date:>=&searchJoin=AND&page=1")
    .headers(headers)
    .check(status is 200)
}
