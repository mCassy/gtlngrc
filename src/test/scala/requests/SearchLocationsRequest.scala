package requests

import config.Config.app_url
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object SearchLocationsRequest {
  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "accept-encoding" -> "gzip, deflate, br",
    "locale" -> "en"
  )
  val get_locations = http("Search locations")
    .get(app_url + "/api/v1/web/locations?search=races.sport_id:6,3,5,52,99,26,59,58,55,24,57,56,60,;races.race_date:2020-04-01;races.race_end_date:2030-04-13&searchFields=races.sport_id:in;races.race_date:>=;races.race_end_date:<=&searchJoin=AND&page=1")
    .headers(headers)
    .check(status is 200)
}
