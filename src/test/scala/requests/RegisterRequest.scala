package requests

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import config.Config.app_url
import helpers.JsonHelper.toJson
import helpers.Session

object RegisterRequest {
  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "accept-encoding" -> "gzip, deflate, br",
    "locale" -> "en"
  )

  val credentials = Map(
    "email" -> Session.randomEmail(),
    "password" -> Session.password(),
    "password_confirmation" -> Session.password()
  )

  val get_token = http("Register User")
    .post(app_url + "/api/v1/web/user/register")
    .headers(headers)
    .body(StringBody(toJson(credentials)))
    .check(status is 200)
    .check(jsonPath("$.data.token").saveAs("token"))
}
