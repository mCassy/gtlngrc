package requests

import config.Config.app_url
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object OpenRaceRequest {
  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "accept-encoding" -> "gzip, deflate, br",
    "locale" -> "en"
  )
  val authHeaders = Map(
    "Authorization" -> "Bearer ${token}"
  )
  val get_one_race = http("Open Race")
    .get(app_url + "/api/v1/web/races/${race}?with=distances;location;organizer;route;currency;contact_emails")
    .headers(headers)
    .headers(authHeaders)
    .check(status is 200)
    .check(jsonPath("$..id").saveAs("race"))
}
