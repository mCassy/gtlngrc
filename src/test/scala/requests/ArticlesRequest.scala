package requests

import config.Config.app_url
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object ArticlesRequest {
  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "accept-encoding" -> "gzip, deflate, br",
    "locale" -> "en"
  )
  val get_articles = http("Home Articles")
    .get(app_url + "/api/v1/web/articles?limit=12&page=1")
    .headers(headers)
    .check(status is 200)
}
