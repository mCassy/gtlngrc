package requests

import config.Config.app_url
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object SearchRaceRequest {
  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "accept-encoding" -> "gzip, deflate, br",
    "locale" -> "en"
  )
  val get_race = http("Search races")
    .get(app_url + "/api/v1/web/races?search=sport_id:6,3,5,52,99,26,59,58,55,24,57,56,60,;race_end_date:2030-04-13+00:00&searchFields=sport_id:in;race_end_date:<&with=distances;location;route&limit=9&orderBy=race_date&sortedBy=desc&page=4&searchJoin=AND")
    .headers(headers)
    .check(status is 200)
    .check(jsonPath("$..id").saveAs("race"))
}
