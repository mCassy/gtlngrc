package requests

import config.Config.app_url
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object TotalDistancesRequest {
  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "accept-encoding" -> "gzip, deflate, br",
    "locale" -> "en"
  )
  val get_total_distances = http("Home Total Distances")
    .get(app_url + "/api/v1/web/distance/count")
    .headers(headers)
    .check(status is 200)
}
