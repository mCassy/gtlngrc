package requests

import config.Config.app_url
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object SportsListRequest {
  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "accept-encoding" -> "gzip, deflate, br",
    "locale" -> "en"
  )
  val get_sport_list = http("Home Sports")
    .get(app_url + "/api/v1/web/sports?limit=-1&page=1")
    .headers(headers)
    .check(status is 200)
}
