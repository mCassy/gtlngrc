package requests

import config.Config.app_url
import helpers.{JsonHelper, Session}
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object UpdateProfileRequest {
  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "accept-encoding" -> "gzip, deflate, br",
    "locale" -> "en"
  )
  val authHeaders = Map(
    "Authorization" -> "Bearer ${token}"
  )
  val profileData = Map(
    "firstname" -> Session.randomName(),
    "lastname" -> Session.randomName(),
    "country_id" -> 215,
    "gender" -> 0,
    "city" -> "City",
    "birthday" -> "2001-10-10",
    "post_code" -> "AB 23431",
    "address" -> "Address street 12",
    "telephone" -> "4534134565"
  )

  val get_profile = http("Update profile")
    .patch(app_url + "/api/v1/web/user/profile")
    .headers(headers)
    .headers(authHeaders)
    .body(StringBody(JsonHelper.toJson(profileData)))
    .check(status is 200)
    .check(jsonPath("$..id").saveAs("user"))
}
