package requests

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import config.Config.app_url


object LoginRequest {
  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "accept-encoding" -> "gzip, deflate, br",
    "locale" -> "en"
  )
  val authHeaders = Map(
    "Authorization" -> "Bearer ${token}"
  )
  val get_profile = http("Login User")
    .get(app_url + "/api/v1/web/user/profile")
    .headers(headers)
    .headers(authHeaders)
    .check(status is 200)
    .check(jsonPath("$..id").saveAs("user"))
}
