package helpers

import java.util.concurrent.ThreadLocalRandom

import scala.util.Random

object Session {
  def randomEmail(): String = {
    s"test_${ThreadLocalRandom.current.nextInt(Integer.getInteger("EndNumber", 10).toInt)}@yopmail.com"
  }

  def password(): String ={
    "111111"
  }

  def randomName(): String ={
    Random.alphanumeric take 7 mkString
  }
}
